create database juegos2;

use juegos2;

CREATE TABLE `juegos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(40) DEFAULT NULL,
  `idcategoria` varchar(40) DEFAULT NULL,
  `descipcion` varchar(100) DEFAULT NULL,
  `idplataforma` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

delimiter $$
create procedure sp_ingresarjuego(
	in _titulo varchar(40),
    in _idcategoria varchar(40),
    in _descripcion varchar(100),
    in _idplataforma varchar(40)
)
begin
	insert into juegos (titulo, idcategoria, descipcion, idplataforma)
		values (_titulo, _idcategoria, _descripcion, _idplataforma);
end
$$

delimiter $$
create procedure sp_consultarjuego(
)
begin
	select  * from juegos;
end
$$

select * from juegos;

call sp_consultarjuego;

delimiter $$
create procedure sp_actualizarjuego(
	in _id int,
	in _titulo varchar(40),
    in _categoria varchar(40),
    in _descripcion varchar(100),
    in _plataforma varchar(40)
)
begin
	update juegos
		set titulo = _titulo,
        idcategoria = _categoria,
        descipcion = _descripcion,
        plataforma = _plataforma
	where id = _id;
end
$$

use juegos2;

create table if not exists usuarios (
	id int not null auto_increment primary key,
	usuario varchar(20) not null unique,
    contrasena varchar(12) not null
);

-- drop procedure sp_consultarusuario;

delimiter $$
create procedure sp_consultarusuario (
	in _usuario varchar(20),
    in _pass varchar(12)
)
begin
    select count(usuario) usuario from usuarios where usuario = _usuario and contrasena = _pass;
end
$$

call sp_consultarusuario("mguzman", "123456");

insert into usuarios (usuario, contrasena) values ("mguzman", "123456");

select count(usuario) from usuarios where usuario = "mguzman" and contrasena = "123456";
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static JuegosInterfaz.Juegos;

namespace JuegosInterfaz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_ingresar_Click(object sender, EventArgs e)
        {
            String titulo = txtb_titulo.Text;
            String categoria = txtb_categoria.Text;
            String descripcion = txtb_descripcion.Text;
            String plataforma = txtb_plataforma.Text;

            Juegos juego = new Juegos(titulo, categoria, descripcion, plataforma);

            juego.ingresarJuego();

            txtb_titulo.Text = "";
            txtb_categoria.Text = "";
            txtb_descripcion.Text =  "";
            txtb_plataforma.Text = "";
        }

        private void archivoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            DataTable infojuegos = new DataTable();
            Juegos juegos = new Juegos();
            infojuegos = juegos.consultarJuegos();
            dgv_juegos.DataSource = infojuegos;
        }

        private void btn_actualizar_Click(object sender, EventArgs e)
        {
            DataTable infojuegos = new DataTable();
            Juegos juegos = new Juegos();
            infojuegos = juegos.consultarJuegos();
            dgv_juegos.DataSource = infojuegos; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2();
            frm.Show();
            this.Hide();
        }

        private void dgv_juegos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtb_titulo.Text = (string)dgv_juegos.Rows[e.RowIndex].Cells[1].Value;
            txtb_categoria.Text = (string)dgv_juegos.Rows[e.RowIndex].Cells[2].Value;
            txtb_descripcion.Text = (string)dgv_juegos.Rows[e.RowIndex].Cells[3].Value;
            txtb_plataforma.Text = (string)dgv_juegos.Rows[e.RowIndex].Cells[4].Value;

        }
    }
}

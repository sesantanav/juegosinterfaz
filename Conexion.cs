﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace JuegosInterfaz
{
    /// <summary>
    /// Clase conexion a la base de datos
    /// </summary>
    class Conexion
    {
        public MySqlConnection conn;
        public String query;

        static String server = ConfigurationManager.AppSettings["host"];
        static String database = ConfigurationManager.AppSettings["nombrebd"];
        static String user = ConfigurationManager.AppSettings["usuario"];
        static String password = ConfigurationManager.AppSettings["password"];

        public String connecString = String.Format("server={0};uid={1};pwd={2};database={3}",
                                                    server, user, password, database);

        public void setQuery(String _query)
        {
            this.query = _query;
        }

        /// <summary>
        /// Metodo para ejecutar consultas en la base datos
        /// </summary>
        public void ejecutarConsulta()
        {
            try
            {
                using(conn = new MySqlConnection(connecString))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(this.query, conn);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error {0}", ex.Message);

            }
        }

        public DataTable consultar()
        {
            DataTable dt = new DataTable();

            try
            {
                using(conn = new MySqlConnection(connecString))
                {
                    conn.Open();
                    MySqlDataAdapter adapter = new MySqlDataAdapter(this.query, conn);
                    adapter.Fill(dt);

                    return dt;
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error {0}", ex.Message);
                throw;
            }
        }


        public MySqlDataReader consultarDatos()
        {

            try
            {
                using(conn = new MySqlConnection(connecString))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(this.query, conn);
                    MySqlDataReader datos = cmd.ExecuteReader();

                    return datos;
                }
            }
            catch (MySqlException ex) 
            {
                MessageBox.Show("Error {0}", ex.Message);
                throw;
            }
        }
        
    }
}

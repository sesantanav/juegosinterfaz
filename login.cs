﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

using static JuegosInterfaz.Conexion;
using static JuegosInterfaz.usuario;

namespace JuegosInterfaz
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void btn_ingresar_Click(object sender, EventArgs e)
        {
            String usuario = txtb_usuario.Text;
            String pass = txtb_contrasena.Text;

            bool validacion = false;

            usuario validar = new usuario(usuario, pass);

            validacion = validar.validarUsuario();
 
            if (validacion = true)
            {
                Form1 frm = new Form1();
                frm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario no existe");
            }
            
        }
    }
}

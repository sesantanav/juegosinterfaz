﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

namespace JuegosInterfaz
{
    class usuario
    {
        public String user { get; set; }
        public String pass { get; set; }

        public usuario() { }

        public usuario(String _user, String _pass) 
        {
            this.user = _user;
            this.pass = _pass;
        }
        public bool validarUsuario()
        {
            DataTable datos = new DataTable();

            Conexion conectar = new Conexion();

            String queryusuario = String.Format("call sp_consultarusuario('{0}', '{1}')", this.user, this.pass);

            conectar.setQuery(queryusuario);
            datos = conectar.consultar();

            String resultado = (string)datos.Rows[0].ToString();

            if (resultado == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

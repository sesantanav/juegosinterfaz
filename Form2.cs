﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static JuegosInterfaz.Conexion;

namespace JuegosInterfaz
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load_1(object sender, EventArgs e)
        {
            DataTable dtJuegos = new DataTable();
            Juegos listadoJuegos = new Juegos();

            dtJuegos = listadoJuegos.consultarJuegos();

            cbxJuegos.DataSource = dtJuegos;
            cbxJuegos.DisplayMember = "idcategoria";
            cbxJuegos.ValueMember = "id";
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            DataTable dtJuegos = new DataTable();
            Juegos listadoJuegos = new Juegos();

            dtJuegos = listadoJuegos.consultarJuegos();

            cbxJuegos.DataSource = dtJuegos;
            cbxJuegos.DisplayMember = "idcategoria";
            cbxJuegos.ValueMember = "id";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

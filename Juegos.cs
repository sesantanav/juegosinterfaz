﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using static JuegosInterfaz.Conexion;

namespace JuegosInterfaz
{
    public class Juegos
    {
        // atributos
        public String titulo;
        public String categoria;
        public String descripcion;
        public String plataforma;

        // constructor

        public Juegos(String _titulo, String _categoria, String _desc, String _plataforma)
        {
            this.titulo = _titulo;
            this.categoria = _categoria;
            this.descripcion = _desc;
            this.plataforma = _plataforma;
        }
        public Juegos() { }

        // accesadores
        

        // metodos
        public void ingresarJuego()
        {
            Conexion conectar = new Conexion();

            String query = String.Format("call sp_ingresarjuego(('{0}', '{1}', '{2}', '{3}');", 
                this.titulo, this.categoria, this.descripcion, this.plataforma);

            // String query2 = "call sp_ingresarjuego('" + titulo + "',' "+ categoria + "',' " + descripcion + "',' " + plataforma + "')";

            conectar.setQuery(query);

            conectar.ejecutarConsulta();

        }

        public void actualizarJuego(string _id)
        {
            Conexion conectar = new Conexion();
            String query = String.Format("call sp_actualizarjuego('{0}', '{1}', '{2}', '{3}', {4});", 
               _id ,this.titulo, this.categoria, this.descripcion, this.plataforma);
            conectar.setQuery(query);
            conectar.ejecutarConsulta();
        }

        public DataTable consultarJuegos()
        {
            DataTable juegos = new DataTable();

            String query = "call sp_consultarjuego;";

            Conexion consultar = new Conexion();
            consultar.setQuery(query);

            juegos = consultar.consultar();

            return juegos;
        }

    }

}
